#include "SQL_CANID_GEN.hpp"

SQL_CANID_GEN::SQL_CANID_GEN(QObject *parent,SQL_CANID *parentSQL) : QObject(parent)
{
    CANId = parentSQL;
}

void SQL_CANID_GEN::genHeaderFile(int Device)
{
    QStringList Header;
    QVariant DevVar;
    QVariant MsgVar;
    QVariant TopFieldVar;
    QVariant BotFieldVar;


    DevVar = CANId->giveDevId(Device);
    Header.append(QString("#ifndef %1_CAN_H").arg(DevVar.toString()));
    Header.append(QString("#define %1_CAN_H").arg(DevVar.toString()));
    Header.append("");
    Header.append("#include <stdbool.h>");
    Header.append("#include <stdint.h>");
    Header.append("#include <can-ids/CAN_IDs.h>");
    Header.append("");
    foreach (QString Message,CANId->giveMessages(Device)) {
        MsgVar = CANId->giveMsgId(CANId->giveDevId(Device),Message);
        Header.append(QString("#define MSG_ID_%1_%2 %3").arg(DevVar.toString())
                                                        .arg(Message.toUpper())
                                                        .arg(MsgVar.toString()));
    }
    Header.append("");

    // Structure Generation
    foreach (QString Message,CANId->giveMessages(Device)) {
        Header.append("typedef struct {");
        foreach (QString Top, CANId->giveTopFieldsFromMes(CANId->giveCanId(DevVar.toString(),Message))) {
            TopFieldVar = CANId->giveFieldId(CANId->giveCanId(DevVar.toString(),Message),Top);
            if (!CANId->giveBotFieldsFromMes(CANId->giveCanId(DevVar.toString(),Message),TopFieldVar.toInt()).isEmpty()) {
                Header.append("    union {");
                Header.append("        struct {");
                foreach (QString Bot, CANId->giveBotFieldsFromMes(CANId->giveCanId(DevVar.toString(),Message),TopFieldVar.toInt())) {
                    int BotId = CANId->giveFieldId(CANId->giveCanId(DevVar.toString(),Message),Bot);
                    Header.append(QString("                %1 %2:%3;")
                                  .arg(CANId->giveFieldType(BotId))
                                  .arg(Bot)
                                  .arg(CANId->giveFieldSize(BotId)));
                }
                Header.append("            };");
                Header.append(QString("            %1 %2;")
                              .arg(CANId->giveFieldType(CANId->giveFieldId(CANId->giveCanId(DevVar.toString(),Message),Top)))
                              .arg(Top));
                Header.append("    };");
            } else {
                Header.append(QString("            %1 %2;")
                              .arg(CANId->giveFieldType(CANId->giveFieldId(CANId->giveCanId(DevVar.toString(),Message),Top)))
                              .arg(Top));
            }
        }
        Header.append(QString("} %1_MSG_%2;")
                      .arg(CANId->giveDevId(Device))
                      .arg(Message));
    }
    Header.append("");

    // Main Structure Generation
    Header.append("typedef struct {");
    foreach (QString Message,CANId->giveMessages(Device)) {
        Header.append(QString("    %1_MSG_%2 %3;")
                      .arg(CANId->giveDevId(Device))
                      .arg(Message)
                      .arg(Message.toLower()));
    }
    Header.append(QString("} %1_CAN_Data;").arg(CANId->giveDevId(Device)));
    Header.append("");

    foreach (QString Message,CANId->giveMessages(Device)) {
        Header.append(QString("void parse_%1_message_%2(uint16_t data[4], %3_MSG_%4 *%5);")
                      .arg(CANId->giveDevId(Device).toLower())
                      .arg(Message.toLower())
                      .arg(CANId->giveDevId(Device))
                      .arg(Message)
                      .arg(Message.toLower()));
    }
    Header.append(QString("void parse_can_%1(CANdata message, %2_CAN_Data *data);")
                  .arg(CANId->giveDevId(Device).toLower())
                  .arg(CANId->giveDevId(Device)));

    Header.append("");
    Header.append("#endif");
    foreach (QString String,Header)
        qDebug()<<String;
}

void SQL_CANID_GEN::genCodeFile(int Device)
{
    QStringList Code;
    Code.append("#include <stdbool.h>");
    Code.append("#include <stdint.h>");
    Code.append("#include <can-ids/CAN_IDs.h>");
    Code.append("");
    foreach (QString Message,CANId->giveMessages(Device)) {
        Code.append(QString("void parse_%1_message_%2(uint16_t data[4], %3_MSG_%4 *%5) {")
                      .arg(CANId->giveDevId(Device).toLower())
                      .arg(Message.toLower())
                      .arg(CANId->giveDevId(Device))
                      .arg(Message)
                      .arg(Message.toLower()));
        foreach (QString Field,CANId->giveTopBotFieldsFromMes(CANId->giveCanId(Device,CANId->giveMsgId(Device,Message)))) {
            Code.append(QString("%1->%2 = data[%3] >> %4 & %5;")
                        .arg(Message.toLower())
                        .arg(Field)
                        .arg(giveDataIndex(CANId->giveFieldStart(CANId->giveFieldName(Field)),
                                           CANId->giveFieldSize(CANId->giveFieldName(Field))))
                        .arg(CANId->giveFieldStart(CANId->giveFieldName(Field))-16*giveDataIndex(CANId->giveFieldStart(CANId->giveFieldName(Field)),
                                                                                                 CANId->giveFieldSize(CANId->giveFieldName(Field))))
                        .arg(CANId->giveFieldSize(CANId->giveFieldName(Field))));
        }
        Code.append("}");
    }
    Code.append("");
    Code.append(QString("void parse_can_%1(CANdata message, %2_CAN_Data *data) {")
                  .arg(CANId->giveDevId(Device).toLower())
                  .arg(CANId->giveDevId(Device)));
    Code.append(QString("   if (message.dev_id != DEVICE_ID_%1) {")
                .arg(CANId->giveDevId(Device)));
    Code.append("       return;");
    Code.append("   }");
    Code.append("");
    Code.append("   switch (message.msg_id) {");
    foreach (QString Message, CANId->giveMessages(Device)) {
        Code.append(QString("       case MSG_ID_%1_%2:")
                    .arg(CANId->giveDevId(Device))
                    .arg(Message.toUpper()));
        Code.append(QString("           parse_%1_message_%2(message.data, &(data->%3);")
                      .arg(CANId->giveDevId(Device).toLower())
                      .arg(Message.toLower())
                      .arg(Message.toLower()));
         Code.append("          break:");
    }
    Code.append("   }");
    Code.append("}");
    foreach (QString String,Code)
        qDebug()<<String;
}

int SQL_CANID_GEN::giveDataIndex(int startBit,int size)
{
    int endBit = startBit + size - 1;

    int startIndex = startBit/16;
    int endIndex = endBit/16;

    if (startIndex == endIndex)
        return startIndex;
    else {
        return startIndex*(10 ^ endIndex);
    }
}
