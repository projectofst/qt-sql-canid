#ifndef SQL_CANID_GEN_HPP
#define SQL_CANID_GEN_HPP

#include <QObject>
#include <QSqlQuery>
#include <QVariant>

#include "SQL_CANID.hpp"

class SQL_CANID_GEN : public QObject
{
    Q_OBJECT
public:
    explicit SQL_CANID_GEN(QObject *parent = nullptr,
                           SQL_CANID *parentSQL = nullptr);
   SQL_CANID *CANId;
   void genHeaderFile(int);
   void genCodeFile(int);

signals:

public slots:

private:
   int giveDataIndex(int,int);

};

#endif // SQL_CANID_GEN_HPP
