#ifndef COM_SQL_CANID_HPP
#define COM_SQL_CANID_HPP

#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "CAN.h"

class SQL_CANID : public QObject
{
    Q_OBJECT

public:
    SQL_CANID();
    virtual ~SQL_CANID() {}
    QSqlDatabase CANId;
    QStringList giveDevices();
    QStringList giveMessages(int);
    QString giveDbName() {return CANId.databaseName();}
    QStringList giveTables() {return CANId.tables(QSql::Tables);}
    QStringList giveViews() {return CANId.tables(QSql::Views);}
    int giveDevId(QString);
    QString giveDevId(int);
    int giveMsgId(int,QString);
    int giveMsgId(QString,QString);
    int giveCanId(int,int);
    int giveCanId(QString,QString);
    QMap<QString,QVector<QVariant>> giveView(QString);
    bool translateMsg(CANdata,QString,int*);
    int bitwiseThis(int64_t Input,int Start,int End);
    void setDatabase(QString);
    QMap<int,QString> giveMsgIds(int Device);
    QStringList giveTopFieldsFromMes(int);
    QStringList giveBotFieldsFromMes(int,int);
    QStringList giveTopBotFieldsFromMes(int);
    int giveFieldId(int,QString);
    int giveFieldName(QString Field);
    QString giveFieldType(int);
    int giveFieldSize(int);
    int giveFieldStart(int);
};

#endif // COM_SQL_CANID_HPP
