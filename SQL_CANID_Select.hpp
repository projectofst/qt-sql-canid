#ifndef SQL_CANID_SELECT_HPP
#define SQL_CANID_SELECT_HPP

#include <QWidget>
#include <QObject>
#include <QSqlDatabase>

#include "SQL_CANID.hpp"

namespace Ui {
class SQL_CANID_Select;
}

class SQL_CANID_Select : public QWidget
{
    Q_OBJECT

public:
    explicit SQL_CANID_Select(QWidget *parent = nullptr,
                              SQL_CANID *parentsql = nullptr);
    ~SQL_CANID_Select();
    SQL_CANID *DataBase;
    void updateViewList();
    void updateDbPath();

public slots:
    void updateTable(QString);

private:
    Ui::SQL_CANID_Select *ui;
};

#endif // SQL_CANID_SELECT_HPP
