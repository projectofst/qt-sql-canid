#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

#include "SQL_CANID.hpp"
#include "SQL_CANID_Select.hpp"
#include "SQL_CANID_GEN.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    SQL_CANID *DB;
    SQL_CANID_Select *DB_UI;
    SQL_CANID_GEN *DB_GEN;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_HPP
