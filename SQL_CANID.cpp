#include "SQL_CANID.hpp"

SQL_CANID::SQL_CANID()
{
    CANId = QSqlDatabase::addDatabase("QSQLITE");
    CANId.setDatabaseName("/Users/diogopereira/Documents/fst-can-interface/sql/MainCAN.db");
    CANId.open();

}

QStringList SQL_CANID::giveDevices()
{
    QSqlQuery test;
    test.exec(QString("SELECT Designation FROM Devices WHERE Designation != 'Nothing'"));
    test.first();
    QStringList Output;
    while (test.isValid()) {
        Output.append(test.value(0).toString());
        test.next();
    }

    return Output;
}

QStringList SQL_CANID::giveMessages(int Dev)
{
    QSqlQuery test;
    test.exec(QString("SELECT Designation FROM Messages WHERE DevId = %1").arg(Dev));
    test.first();
    QStringList Output;
    while (test.isValid()) {
        Output.append(test.value(0).toString());
        test.next();
    }
    return Output;
}

QStringList SQL_CANID::giveTopFieldsFromMes(int CANID)
{
    QSqlQuery test;
    test.exec(QString("SELECT FIELDNAME FROM Struct WHERE CANID = %1 AND PARENTSTRID ISNULL").arg(CANID));
    test.first();
    QStringList Output;
    while (test.isValid()) {
        Output.append(test.value(0).toString());
        test.next();
    }
    return Output;
}

int SQL_CANID::giveFieldId(int CANID,QString Field)
{
    QSqlQuery test;
    test.exec(QString("SELECT STRUCTID FROM Struct WHERE FIELDNAME = '%1' AND CANID = %2").arg(Field).arg(CANID));
    test.first();
    return test.value(0).toInt();
}

QStringList SQL_CANID::giveBotFieldsFromMes(int CANID,int ParentField)
{
    QSqlQuery test;
    test.exec(QString("SELECT FIELDNAME FROM Struct WHERE CANID = %1 AND PARENTSTRID == %2").arg(CANID).arg(ParentField));
    test.first();
    QStringList Output;
    while (test.isValid()) {
        Output.append(test.value(0).toString());
        test.next();
    }
    return Output;
}

QStringList SQL_CANID::giveTopBotFieldsFromMes(int CANID)
{
    QSqlQuery test;
    test.exec(QString("SELECT FIELDNAME FROM Struct WHERE CANID = %1 AND PARENTSTRID IS NOT NULL").arg(CANID));
    test.first();
    QStringList Output;
    while (test.isValid()) {
        Output.append(test.value(0).toString());
        test.next();
    }
    return Output;
}

int SQL_CANID::giveDevId(QString Device)
{
    QSqlQuery test;
    test.exec(QString("SELECT Id FROM Devices WHERE Designation = '").append(Device.append("'")));
    test.first();
    return test.value(0).toInt();
}

QString SQL_CANID::giveDevId(int Device)
{
    QSqlQuery test;
    test.exec(QString("SELECT Designation FROM Devices WHERE Id = %1").arg(Device));
    test.first();
    return test.value(0).toString();
}

int SQL_CANID::giveMsgId(int Dev,QString Message)
{
    QSqlQuery test;
    test.exec(QString("SELECT ID FROM Messages WHERE Designation = '%1' AND DEVID = %2").arg(Message).arg(Dev));
    test.first();
    return test.value(0).toInt();
}

int SQL_CANID::giveMsgId(QString DevStr,QString Message)
{
    QSqlQuery test;
    int Dev = giveDevId(DevStr);
    test.exec(QString("SELECT ID FROM Messages WHERE Designation = '%1' AND DEVID = %2").arg(Message).arg(Dev));
    test.first();
    return test.value(0).toInt();
}

int SQL_CANID::giveCanId(int Dev,int Msg)
{
    QSqlQuery test;
    test.exec(QString("SELECT CANID FROM Messages WHERE ID = %1 AND DEVID = %2").arg(Msg).arg(Dev));
    test.first();
    return test.value(0).toInt();
}

int SQL_CANID::giveCanId(QString DevStr,QString MsgStr)
{
    QSqlQuery test;
    int Msg = giveMsgId(DevStr,MsgStr);
    int Dev = giveDevId(DevStr);
    test.exec(QString("SELECT CANID FROM Messages WHERE ID = %1 AND DEVID = %2").arg(Msg).arg(Dev));
    test.first();
    return test.value(0).toInt();
}

int SQL_CANID::giveFieldName(QString Field)
{
    QSqlQuery test;
    test.exec(QString("SELECT STRUCTID FROM Struct WHERE FIELDNAME = '%1'").arg(Field));
    test.first();
    return test.value(0).toInt();
}

QString SQL_CANID::giveFieldType(int StrId)
{
    QSqlQuery test;
    test.exec(QString("SELECT TYPE FROM Struct WHERE STRUCTID = %1").arg(StrId));
    test.first();
    return test.value(0).toString();
}

int SQL_CANID::giveFieldStart(int StrId)
{
    QSqlQuery test;
    test.exec(QString("SELECT STARTbit FROM Struct WHERE STRUCTID = %1").arg(StrId));
    test.first();
    return test.value(0).toInt();
}

int SQL_CANID::giveFieldSize(int StrId)
{
    QSqlQuery test;
    test.exec(QString("SELECT SIZE FROM Struct WHERE STRUCTID = %1").arg(StrId));
    test.first();
    return test.value(0).toInt();
}

QMap<int,QString> SQL_CANID::giveMsgIds(int Device)
{
    QSqlQuery temp;
    temp.prepare("SELECT Id FROM Messages WHERE DevId = [(Device)]"
                 "VALUES (:Device)");
    temp.bindValue("Device",QVariant());
    temp.exec();
    if (temp.isActive() && temp.isSelect())
        temp.first();

//    while (temp.isValid()) {
//        temp.append(QString("#define MSG_ID_%s_%s").arg("DCU").arg());
//        temp.next();
//    }
}

QMap<QString,QVector<QVariant>> SQL_CANID::giveView(QString ViewName)
{


    QMap<QString,QVector<QVariant>> View;


    return View;
}

bool SQL_CANID::translateMsg(CANdata Input, QString Field, int *Value)
{
    QSqlQuery TranslateQuery;

    bool Output;

    QString Pattern = QString::number((Input.msg_id << 5 )+ Input.dev_id);
    TranslateQuery.exec(QString("SELECT FIELDNAME, STARTbit, ENDbit FROM Struct WHERE CANID = %d AND FIELDNAME = '%s'").arg((Input.msg_id << 5 )+ Input.dev_id).arg(Field));
    TranslateQuery.first();

    if (TranslateQuery.isValid() && TranslateQuery.value(0).toString() == Field) {
        int64_t Data = (Input.data[0] << 48 )+(Input.data[1] << 32)+(Input.data[2] << 16)+Input.data[3];
        bitwiseThis(Data,TranslateQuery.value(1).toInt(),TranslateQuery.value(2).toInt());
        return true;
    } else {
        return false;
    }
}

int SQL_CANID::bitwiseThis(int64_t Input,int Start,int End)
{
    int Output;

    Output = Input << Start & End;

    return Output;
}

void SQL_CANID::setDatabase(QString AbsPath)
{
    CANId.close();
    CANId = QSqlDatabase::addDatabase("QSQLITE");
    CANId.setDatabaseName(AbsPath);
    CANId.open();
}
