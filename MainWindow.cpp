#include "MainWindow.hpp"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    DB = new SQL_CANID;

    DB_UI = new SQL_CANID_Select(ui->Database,DB);
    QVBoxLayout LayDB_UI(ui->Database);
    LayDB_UI.addWidget(DB_UI);
}

MainWindow::~MainWindow()
{
    delete ui;
}
