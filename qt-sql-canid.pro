#-------------------------------------------------
#
# Project created by QtCreator 2019-01-23T02:28:12
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt-sql-canid
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

INCLUDEPATH +=\
    dbs/\

SOURCES += \
        main.cpp \
        MainWindow.cpp \
        SQL_CANID.cpp \
        SQL_CANID_GEN.cpp \
        SQL_CANID_Select.cpp \
    CAN.c

HEADERS += \
        MainWindow.hpp \
        SQL_CANID.hpp \
        SQL_CANID_GEN.hpp \
        SQL_CANID_Select.hpp \
    CAN.h

FORMS += \
        MainWindow.ui \
        SQL_CANID_Select.ui
